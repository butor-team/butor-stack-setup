//debugger
try {
	App.Bundle.override('common', {
		'copyright': {'en' : '&copy; Set Me Inc.', 'fr' : '&copy; Configurer Moi Inc.'},
		'Portal': {'en' : 'Portal', 'fr' : 'Portail'},
		'contact-us-url': {'fr' : '/wl?h=contact-us-fr.html', 'en' : '/wl?h=contact-us-en.html'}
	});

} catch (err) {
	LOGGER.error(err);
}

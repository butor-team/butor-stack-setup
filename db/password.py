#!/usr/bin/python
import hashlib, binascii
from array import array
from struct import pack,unpack
import os,sys, getpass

if len(sys.argv) == 2 :
	# convert random 8 bytes to long 
	salt_n=unpack('>q',os.urandom(8))[0];
	password=sys.argv[1];
elif len(sys.argv) == 3:
	password=sys.argv[1];
	salt_n=long(sys.argv[2]);
else :
	password=getpass.getpass();

hf = hashlib.sha512();


# convert to absolute val :
if salt_n < 0 :
	salt_n = salt_n * -1;

#get the salt bytes (big endian for representation in the final token)
salt_bytes = bytearray(pack('>q',salt_n));

# convert signed Long.MAX_VALUE to byte array (little endian for hashing) 
max_long_bytes=bytearray(pack('<q',0x7fffffffffffffff))


# hash each bytes of char
for k in bytearray(password):
	# convert to k + 0x00, for Java Compatibility (unicode 16 bits)
	k=pack("bb",k,0x00);
	# update the Hash function
	hf.update(k)

#update the hash function with salt + Long.max_value 
# we need to convert salt to little endian for hashing)
hf.update(bytes(pack('<q',salt_n)));
hf.update(bytes(max_long_bytes));

# output SALT + SHA512(PASSWORD+SALT+Long.MAX_VALUE)
print binascii.hexlify(bytes(salt_bytes))+hf.hexdigest()

#print "SHA512 : ",hf.hexdigest()
#print "Salt Number : ", salt_n;
#print "Salt Hex :",binascii.hexlify(salt_bytes);

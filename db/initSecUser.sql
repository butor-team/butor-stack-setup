-- Users
DELETE FROM `secUser`;
INSERT INTO `secUser` (`id`,`active`,`firmId`,`creationDate`,`displayName`,`fullName`,`email`,`firstName`,`lastLoginDate`,`lastName`,`pwd`,`resetInProgress`,`missedLogin`,`lastQ`,`revNo`,`stamp`,`userId`) VALUES 
	('__EMAIL__',true,1,CURRENT_TIMESTAMP,'__FIRST_NAME__ __LAST_NAME__','__FIRST_NAME__ __LAST_NAME__','__EMAIL__','__FIRST_NAME__',NULL,'__LAST_NAME__','__PASSWORD_HASH__',false, 0, 0,1,CURRENT_TIMESTAMP,'load'),
	('portal',true,1,CURRENT_TIMESTAMP,'portal backend system user','portal backend system user','portal','portal',NULL,'portal','-',false, 0, 0,1,CURRENT_TIMESTAMP,'load');

UPDATE `secGroup` SET member = '__EMAIL__' where member = 'admin@this.portal';
UPDATE `secAuth` SET who = '__EMAIL__' where who = 'admin@this.portal';


#!/bin/bash -e

WGET="wget q-O-"

function fetch {
	wget $1
	cat `basename $1`
}

if [[ -f "/.dockerinit" ]]; then
	# trick to make dialog work in a Docker container
	# https://github.com/docker/docker/issues/728
	exec >/dev/tty 2>/dev/tty </dev/tty 
fi

DBNAME="PORTAL";
DEST="/tmp/script.sql"

BUTOR_AUTH_DAO_VERSION="tip"
BUTOR_ATTRSET_DAO_VERSION="tip"
BUTOR_PORTAL_DAO_VERSION="tip"


DBNAME=$(dialog --inputbox "DB name"  8 30 "${DBNAME}"  --stdout)
[[ "$?" != 0 ]] && exit 1;

ID=$(dialog --inputbox "Admin email"  8 30 --stdout)
[[ "$?" != 0 ]] && exit 1;

FIRST_NAME=$(dialog --inputbox "Admin First name"  8 30 --stdout)
[[ "$?" != 0 ]] && exit 1;

LAST_NAME=$(dialog --inputbox "Admin Last name"  8 30 --stdout)
[[ "$?" != 0 ]] && exit 1;

BUTOR_AUTH_DAO_VERSION=$(dialog --inputbox "Butor Auth DAO version"  8 30 "${BUTOR_AUTH_DAO_VERSION}" --stdout)
[[ "$?" != 0 ]] && exit 1;

BUTOR_ATTRSET_DAO_VERSION=$(dialog --inputbox "Butor Attrset DAO version"  8 30 "${BUTOR_ATTRSET_DAO_VERSION}" --stdout)
[[ "$?" != 0 ]] && exit 1;

BUTOR_PORTAL_DAO_VERSION=$(dialog --inputbox "Butor Portal DAO version"  8 30 "${BUTOR_PORTAL_DAO_VERSION}" --stdout)
[[ "$?" != 0 ]] && exit 1;

PASSWORD=$(dialog --insecure --passwordbox "Admin password" 10 30 --stdout)
[[ "$?" != 0 || -z "${PASSWORD}" ]] && exit 1;

PORTAL_USER_PWD=$(dialog --insecure --passwordbox "db portal user password" 10 30 --stdout)
[[ "$?" != 0 || -z "${PORTAL_USER_PWD}" ]] && exit 1;


DEST=$(dialog --inputbox "Destination script"  8 50 "${DEST}"  --stdout)
[[ "$?" != 0 ]] && exit 1;

if [[ -e "${DEST}" ]] 
then
	dialog --yesno "File ${DEST} exists. Do you want to delete?" 8 50
	if [[ "$?" != 0 ]] 
	then
		exit 1
	fi
fi
PASSWORD_HASH=$(./password.py $PASSWORD)
echo "CREATE DATABASE IF NOT EXISTS ${DBNAME};USE ${DBNAME};" > ${DEST}

printf "\n-- ################## butor-auth-db.sql\n"  >> ${DEST}
fetch https://bitbucket.org/butor-team/butor-auth-dao/raw/${BUTOR_AUTH_DAO_VERSION}/src/main/resources/butor-auth-db.sql >> ${DEST}

printf "\n-- ################## butor-auth-db-init-secFunc.sql\n"   >> ${DEST}
fetch  https://bitbucket.org/butor-team/butor-auth-dao/raw/${BUTOR_AUTH_DAO_VERSION}/src/main/resources/ddl/butor-auth-db-init-secFunc.sql >> ${DEST}

printf "\n-- ################## attrset-db.sql\n"  >> ${DEST}
fetch https://bitbucket.org/butor-team/butor-attrset-dao/raw/${BUTOR_ATTRSET_DAO_VERSION}/src/main/resources/attrset-db.sql >> ${DEST}

printf "\n-- ################## portal-init-auth.sql\n"  >> ${DEST}
fetch https://bitbucket.org/butor-team/butor-portal-dao/raw/${BUTOR_PORTAL_DAO_VERSION}/src/main/resources/init/portal-db-init-auth.sql >> ${DEST}

printf "\n-- ################## portal-init-secFunc.sql\n"  >> ${DEST}
fetch https://bitbucket.org/butor-team/butor-portal-dao/raw/${BUTOR_PORTAL_DAO_VERSION}/src/main/resources/init/portal-db-init-secFunc.sql >> ${DEST}

printf "\n-- ################## portal-init-attrSet.sql\n"  >> ${DEST}
fetch https://bitbucket.org/butor-team/butor-portal-dao/raw/${BUTOR_PORTAL_DAO_VERSION}/src/main/resources/init/portal-init-attrSet.sql >> ${DEST}

printf "\n-- ################## portal-init-secFirm.sql\n"  >> ${DEST}
fetch https://bitbucket.org/butor-team/butor-portal-dao/raw/${BUTOR_PORTAL_DAO_VERSION}/src/main/resources/init/portal-init-secFirm.sql >> ${DEST}

#printf "\n-- ################## portal-init-secUser.sql\n"  >> ${DEST}
#fetch https://bitbucket.org/butor-team/butor-portal-dao/raw/${BUTOR_PORTAL_DAO_VERSION}/src/main/resources/init/portal-init-secUser.sql >> ${DEST}
#printf "\n-- ################## portal-db-user.sql\n"  >> ${DEST}
#fetch https://bitbucket.org/butor-team/butor-portal-dao/raw/${BUTOR_PORTAL_DAO_VERSION}/src/main/resources/db_user/portal-db-user.sql >> ${DEST}

cat  initSecUser.sql createDBUser.sql >> ${DEST}

# I KNOW that is look ugly use multiple sed command
# sed with many -e switch works well on Linux but not on OSX!
sed -i -e "s/__DBNAME__/${DBNAME}/g" ${DEST}
sed -i -e "s/__EMAIL__/${ID}/g" ${DEST}
sed -i -e "s/__FIRST_NAME__/${FIRST_NAME}/g" ${DEST}
sed -i -e "s/__LAST_NAME__/${LAST_NAME}/g" ${DEST}
sed -i -e "s/__PASSWORD_HASH__/${PASSWORD_HASH}/g" ${DEST}
sed -i -e "s/__PASSWORD__/${PORTAL_USER_PWD}/g" ${DEST}

# set portal system db user pwd
echo -n ${PORTAL_USER_PWD} > ../conf/pwd/db.portal.pwd

clear
echo "You can now use a tool to run the script in your DB"
echo "e.g : mysql -uroot -p --default-character-set=utf8 < ${DEST}"

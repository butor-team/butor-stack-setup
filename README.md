#Butor Stack Setup
The Web Portal, Single Sign On (SSO) and the Access management app are the main components of Butor Platform. They are ready for production and allow firms to quickly build distributed and secured enterprise level applications.

Butor Platform is an open-source project, built with [Butor Stack](http://www.butor.com/#m=view&uid=f565103e-90b3-42cc-9523-7084118aafc9), this platform allows developers to build secure and professional single page web applications rapidly.

#Linux install - Prerequisites
* Java 7 JDK (Not tested with Java 8 yet)
* MySQL (>5) or MariaDB (Not tested with other RDBMS)
* SMTP
* Maven 3 (Tested with 3.2)
* Mercurial (If you intend to install from source)
* wget

IMPORTANT :
**If you have just installed MySQL, do not forget to run mysql_secure or mysql_secure_installation to get rid of anonymous user and provide pwd to root user**

#Prepare the backend server
Assuming you have a barebone centos 6.6 server or VM.

setup smtp on the host

install mysql (or maria db)

```
yum install mysql-server wget tar python unzip dialog

#follow the instructions for your distribution to install MySQL
```
install init-script package

```
rm -rf /tmp/butor-team-butor-stack-setup*;\
wget https://bitbucket.org/butor-team/butor-stack-setup/get/tip.tar.gz -O - | \
tar xzf - -C /tmp && cd /tmp/butor-team-butor-stack-setup*/db && ./setup-db.sh
```

Assuming you have created the sql script in /tmp/script.sql

```
mysql -uroot -p --default-character-set=utf8 < /tmp/script.sql
```

#Tomcat (or another webapp container) Installation
Tested with Tomcat 7.0.57. Go fetch the latest Apache Tomcat package and unpack in a directory. The application will be deployed in the webapp folder. You need add a System Property "conf.dir" to point to the directory where the application configuration is. You will learn how to configure the Butor Portal later on.

To do so with Tomcat, you need to modify bin/setenv.sh (or bin/setenv.bat)

Example, **setenv.sh** with /opt/butor/conf as a config directory
```
export JAVA_OPTS =”-Dconf.dir=/opt/butor/conf -Denv=dev -Dlog.dir=/tmp/”
```

#Build and install SSO
Download a binary distribution or build yourself the SSO using Maven.

Before building, make sure your maven installation is working properly.
You will fetch the source using Mercurial.

Also be sure your settings.xml contains sonatype snapshots:
```
<settings xmlns="http://maven.apache.org/SETTINGS/1.0.0"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/SETTINGS/1.0.0
                       http://maven.apache.org/xsd/settings-1.0.0.xsd">
	<servers>
	</servers>

	<profiles>
		<profile>
			<id>nexus</id>
			<!--Enable snapshots for the built in central repo to direct -->
			<!--all requests to nexus via the mirror -->
			<pluginRepositories>
				<pluginRepository>
					<name>oss.sonatype.org</name>
					<id>oss.sonatype.org</id>
					<url>http://oss.sonatype.org/content/groups/public</url>
				</pluginRepository>
			</pluginRepositories>
		</profile>

		<profile>
			<id>allow-snapshots</id>
			<activation>
				<activeByDefault>true</activeByDefault>
			</activation>
			<repositories>
				<repository>
					<id>snapshots-repo</id>
					<url>https://oss.sonatype.org/content/repositories/snapshots</url>
					<releases>
						<enabled>false</enabled>
					</releases>
					<snapshots>
						<enabled>true</enabled>
					</snapshots>
				</repository>
			</repositories>
		</profile>

	</profiles>
	<activeProfiles>
		<!--make the profile active all the time -->
		<activeProfile>nexus</activeProfile>
	</activeProfiles>
</settings>
```

```
hg clone https://bitbucket.org/butor-team/sso
cd sso
mvn -P embedded-services package 
```

If the build is successful, the war will be present in the target directory with the name "sso.war". You should copy the war to the webapps directory of the Tomcat you installed


#Build and install the Application Security
Download a binary distribution or build yourself the Application Security using Maven.

Before building, make sure your maven installation is working properly.
You will fetch the source using Mercurial

```
hg clone https://bitbucket.org/butor-team/bsec
cd bsec
mvn -P embedded-services package 
```

If the build is successful, the war will be present in the target directory with the name "bsec.war". You should copy the war to the webapps directory of the Tomcat you installed


#Build and install the Portal
Download a binary distribution or build yourself the Application Security using Maven.

Before building, make sure your maven installation is working properly.
You will fetch the source using Mercurial

```
hg clone https://bitbucket.org/butor-team/portal
cd portal
mvn -P embedded-services package 
```

If the build is successful, the war will be present in the target directory with the name "portal.war". You should copy the war to the webapps directory of the Tomcat you installed


#Prepare the config
We assume that your configuration is under /opt/butor/conf
The webapps configuration will be under /opt/butor/conf/webapps
Usually the default configuration should be good enough. 
It's a good practice to put your configuration under a source control so that you can keep track of your changes. 

You can copy a skeleton for the previously cloned project butor-stack-setup.

IMPORTANT :
```
set the password of the db user portal in the file conf/pwd/db.portal.pwd
```


#Start and test the portal
Once everything is configured you can start tomcat and logon to (http://localhost:8080)
To login, you will need to use the user and the password you configured in the "Prepare the backend server section". 
![Alt text](sso.png)


You should end up in the "Security" application.

![Alt text](portal.png)

If something goes wrong, you need to go look at the logs under your tomcat installation.

You are ready to start coding your first application using the Butor framework!